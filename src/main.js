import 'promise-polyfill/src/polyfill';
import 'promise-polyfill/src/allSettled';
import 'promise-polyfill/src/finally';
import 'promise-polyfill/src/index';


import 'core-js/features/set';
import "core-js/features/map";
import "core-js/features/object/assign";
import "core-js/features/array/from";
import "core-js/features/array-buffer";

import 'whatwg-fetch';

import App from './App.svelte';
import { format } from './GevUtil'



function getParameterByName(name, url = window.location.href) {
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

const retailId = function() {
    var r = (window.location.pathname.split("/").slice(-1)[0]);
    r = (r == "index.html" || r == "") ? 801000 : r; //TO DO ELIMINARE PRIMA DI RILASCIO
    return r
}();




const app = new App({
    target: document.body,
    props: {
        urlGetQr: format('http://193.122.59.163/retail-terminal-monitor-be/qrcode?retailId={retailId}', { retailId: retailId }),
        urlGetBiglietti: 'http://193.122.59.163/retail-terminal-monitor-be/tickets?retailId={retailId}&pageSelected={page}&ticketPerPage=' + getParameterByName("rows"),
        urlVendi: "http://193.122.59.163/retail-terminal-monitor-be/qrcode",
        urlTicketStatusTemplate: 'http://193.122.59.163/retail-terminal-monitor-be/ticket-status?retailerId={retailId}&ticketId={id}',
        retailId: retailId,
        elementiPerPagina: getParameterByName("rows"),
        applicazione: getParameterByName("app")
    }
});

export default app;
function format(template, data) {
    const pattern = /{\s*(\w+?)\s*}/g; // {property}
    //return template.replace(pattern, (_, token) => data[token] || '');
    return template.replace(pattern, function(_, token) {
        return data[token] || "";
    });
}

function suppress(fun) {

    try {
        fun()
    } catch (err) {
        //console.error(err);
    }
}

async function getBiglietti(url) {

    const res = await fetch(url, {
        method: "GET",
        headers: { "Content-Type": "application/json" },
    });
    const json = await res.json();
    return json;
}

export { format, suppress, getBiglietti };